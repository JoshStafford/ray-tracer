import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class Image {

    private BufferedImage img;

    Image(){}

    Image(int width, int height){

        img = new BufferedImage(width, height,
                                BufferedImage.TYPE_INT_RGB);
        
    }

    public boolean setPixel(Vector2 pos, Colour col) {

        try {
            img.setRGB(pos.x, pos.y, col.get());
        } catch(Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;

    } 

    public boolean fill(Colour col) {

        Vector2 pos = new Vector2();

        for(int i = 0; i < img.getWidth(); i++) {

            for(int j = 0; j < img.getHeight(); j++) {

                pos.x = i;
                pos.y = j;

                if(!this.setPixel(pos, col)) {
                    return false;
                }

            }

        }

        return true;

    }

    public boolean save_image(String filename, String extension) {

        File f = new File(filename + "." + extension);
        try{
            ImageIO.write(img, extension, f);
            return true;
        } catch(Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

    }

    public boolean square(Vector2 start, Vector2 end, Colour col) {

        Vector2 pos = new Vector2();

        for(int i = start.x; i < end.x; i++) {

            for(int j = start.y; j < end.y; j++) {
                pos.x = i;
                pos.y = j;
                if(!this.setPixel(pos, col)){
                    return false;
                }

            }

        }

        return true;

    }

    public int getWidth(){
        return img.getWidth();
    }

    public int getHeight(){
        return img.getHeight();
    }

}
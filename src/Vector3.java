import java.lang.Math;

public class Vector3 {

    float x, y, z;

    Vector3(){}
    
    Vector3(float _x, float _y, float _z) {

        x = _x;
        y = _y;
        z = _z;

    }

    public float get_magnitude(){

        return (float)Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);

    }

    public void normalize() {

        float mag = 1 / this.get_magnitude();

        this.x = this.x * mag;
        this.y = this.y * mag;
        this.z = this.z * mag;

    }

    public Vector3 normalized() {

        float mag = 1 / this.get_magnitude();

        float nx = this.x * mag;
        float ny = this.y * mag;
        float nz = this.z * mag;

        return new Vector3(nx, ny, nz);

    }

    public static Vector3 delta(Vector3 p1, Vector3 p2) {

        float dx = p2.x - p1.x;
        float dy = p2.y - p1.y;
        float dz = p2.z - p1.z;

        return new Vector3(dx, dy, dz);

    }

    public static float dot(Vector3 p1, Vector3 p2) {

        return (p1.x*p2.x) + (p1.y*p2.y) + (p1.z*p2.z);

    }

    public static Vector3 scalar(Vector3 v, float scale) {

        v.x *= scale;
        v.y *= scale;
        v.z *= scale;

        return v;

    }

    public static Vector3 zero(){
        return new Vector3(0, 0, 0);
    }

    public static Vector3 add(Vector3 v1, Vector3 v2) {

        return new Vector3(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z);

    }

    public boolean equals(Vector3 v) {

        return this.x == v.x && this.y == v.y && this.z == v.z;

    }

}
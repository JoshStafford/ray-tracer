public class Scene {

    Object[] environment = new Object[2];
    int next = 0;

    Scene(){}

    public void add(Object obj) {
        environment[next] = obj;
        next++;
    }

    // Function always returning last object in array
    // FIX

    public Ray_hit closest_intersection(Ray r) {

        Ray_hit nearest = new Ray_hit();

        for(int i = 0; i < this.environment.length; i++) {

            Object entity = this.environment[i];
            Ray_hit rhit = new Ray_hit();
            boolean intersect = entity.intersects(r, rhit);

            if(intersect) {

                if(rhit.t < nearest.t){
                    nearest = rhit;
                }

            }

        }

        return nearest;


    }


}
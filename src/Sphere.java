// Intersection algorithm
// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection

public class Sphere extends Object {

    Vector3 pos;
    float radius;
    
    Sphere(){}

    Sphere(Vector3 p, float r) {

        pos = p;
        radius = r;

    }

    public boolean intersects(Ray r, Ray_hit rhit){

        Vector3 L = Vector3.delta(r.origin, this.pos);
        float tca = Vector3.dot(L, r.dir);

        float d2 = Vector3.dot(L, L) - tca*tca;
        if(d2 > radius*radius || d2 < -1*(radius*radius)) { return false; }

        float thc = (float)Math.sqrt(radius*radius - d2);

        float t0 = tca - thc;
        float t1 = tca + thc;

        if(t0 > t1) { t0 = t1; }
        if(t0 < 0){
            t0 = t1;
            if(t0 < 0) { return false; }
        }

        Vector3 raypos = Vector3.add(r.origin, Vector3.scalar(r.dir, t0));
        rhit.update(raypos, t0, this);
        return true;
    }

    public Vector3 get_normal(Ray_hit hit) {

        return Vector3.delta(this.pos, hit.point).normalized();

    }

    public boolean equals(Object obj) {

        if(!(obj instanceof Sphere)) {
            return false;
        }
        Sphere s = (Sphere)obj;
        return this.pos.equals(s.pos) && this.radius == s.radius;

    }

}
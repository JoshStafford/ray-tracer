
public class Ray_hit {

    Vector3 point;
    boolean hit;
    float t;
    Object obj;

    Ray_hit(Vector3 pos, float t0, Object obj_hit){
        this.point = pos;
        this.hit = true;
        this.t = t0;
        this.obj = obj_hit;
    }

    Ray_hit(){
        this.hit = false;
        this.t = Float.POSITIVE_INFINITY;
    }

    public void update(Vector3 pos, float t0, Object obj_hit){
        this.point = pos;
        this.hit = true;
        this.t = t0;
        this.obj = obj_hit;
    }



}
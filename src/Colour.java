public class Colour {
    
    int r;
    int g;
    int b;

    Colour(){}

    Colour(int _r, int _g, int _b) {

        r = _r;
        g = _g;
        b = _b;

    }

    public int get() {

        int col = (r << 16) | (g << 8) | b;
        return col;

    }

    public static Colour red(){
        return new Colour(255, 0, 0);
    }

    public static Colour green(){
        return new Colour(0, 255, 0);
    }

    public static Colour blue(){
        return new Colour(0, 0, 255);
    }

    public void mult(float val) {

        this.r *= val;
        this.g *= val;
        this.b *= val;

    }



}
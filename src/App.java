public class App {

    public static void main(String[] args) {

        Vector3 light_v = new Vector3(1.5f, 0.5f, 0);
        Light light = new Light(light_v);
        Image img = new Image(500, 500);
        img.fill(Colour.blue());
        Camera cam = new Camera(new Vector3(0.5f, 0.5f, 0));
        Sphere test = new Sphere(new Vector3(0.4f, 0.5f, 2f), 0.2f);
        Sphere test1 = new Sphere(new Vector3(0.4f, 0.2f, 2f), 0.2f);
        // Sphere test2 = new Sphere(new Vector3(0.3f, 0.6f, 2f), 0.2f);
        Scene scene = new Scene();
        scene.add(test);
        scene.add(test1);
        

        for(int i = 0; i < img.getHeight(); i++) {

            for(int j = 0; j < img.getWidth(); j++) {

                Ray r = cam.gen_ray((float)j / (float)img.getWidth(), (float)i / (float)img.getHeight());
                // System.out.println(Integer.toString(j) + " : " + Integer.toString(i));
                Ray_hit intersects = scene.closest_intersection(r);
                if(intersects.hit){
                    // if(intersects.obj.equals(test)){
                    //     System.out.println("Found one");
                    // }
                    // Calculating light
                    Vector3 difference = Vector3.delta(intersects.point, light.pos);
                    Vector3 normal = intersects.obj.get_normal(intersects);
                    difference.normalize();
                    float intensity = Vector3.dot(normal, difference);
                    if(intensity < 0){ intensity = 0; }
                    Colour col = Colour.red();
                    col.mult(intensity);

                    img.setPixel(new Vector2(j, i), col);
                }
        

            }

        }

        img.save_image("Sphere", "png");

    }

}
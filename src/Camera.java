public class Camera {

    Vector3 origin;

    Camera(){}

    Camera(Vector3 ogn) {

        origin = ogn;

    }

    public Ray gen_ray(float x, float y) {

        Vector3 point = new Vector3(x, 1f-y, 1f);
        Vector3 direction = Vector3.delta(this.origin, point).normalized();
        Ray ray = new Ray(this.origin, direction);

        return ray;

    }

}